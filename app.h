/***************************************************************************//**
 * @brief app.h
 *******************************************************************************
 * # License
 * <b>Copyright 2018 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

#ifndef APP_H_
#define APP_H_

#include "gecko_configuration.h"

/* DEBUG_LEVEL is used to enable/disable debug prints. Set DEBUG_LEVEL to 1 to enable debug prints */
#define DEBUG_LEVEL 0

#include "retargetswo.h"
#include <stdio.h>
#include <stdint.h>

typedef enum {
	RESULT_OK,
	RESULT_FAILED_OVER_LENGTH,
	RESULT_FAILED_ODD_STR_CHAR
}errorcode;
typedef enum{
	GATT_PROCEDURE_STATE_IDLE,
	GATT_PROCEDURE_STATE_READ,
	GATT_PROCEDURE_STATE_WRITE,
	GATT_PROCEDURE_STATE_NOTIFY,
	GATT_PROCEDURE_STATE_INDICATE
}gatt_procedure;

typedef struct {
  uint32_t TEMP_SERVICE;
  uint16_t TEMP_CHARACTERISTIC;
  uint32_t TEMP_VALUE;
  uint32_t TEMP_SERVICE_UIID[];
} TempProfile;

TempProfile tempProfile;

#define UART_ONE_LINE_RECEIVED_EVENT		1
/* Main application */
void appMain(gecko_configuration_t *pconfig);
uint8_t aide_findSubString(char main_string[],const char sub_string[]);
uint8_t ascii2hex(char* string,uint8_t * data);
uint8_t ascii2int(char* data);
uint8_t aide_findNewLine(char main_string[]);
void aide_hex_to_string(uint8_t* in, size_t in_size, uint8_t* out);

#endif


