# BLE UART Communication

This project shows a way to connect with UART module and laptop . It is based on the BLE UART example, and is extended to relay incoming UART packets over the USB-TTL convertor to the host.

## Getting Started

The basic knowledge requirement to perform this project is about UART data transfer and BLE advertising.

### Prerequisites

The software that are to be installed are

```
Simplicity Studio version 4.2
Serial Debug Assistant
Toolchain: GNU Arm Embedded v7.2.1
SDK : Bluetooth SDK v2.13.8.0
```
Hardware
```
1) Blue Gecko BGM13P22 (based on Silicon Labs EFR32BG13 Blue Gecko SoC)
3) Wireless Starter Kit (SLWSTK6101D)
2) USB to TTL (P2H31144A0)
```
## Running the tests

1. Upon startup, the device advertises the UART example with the eddystone beacon . The UART is active waiting for the start frame, which is the character.

2. After the start frame is received, an interrupt is generated and the string "Start Frame" is printed out. An additional interrupt function is set up to read incoming data to the UART.

3. The device goes back into blocking other bytes and waiting for the start frame.


### Advertising

The Eddystone format defines four different packet types. This example shows how to implement the Eddystone-URL packet. The Eddystone-URL packet advertises a URL in a compressed format. For more information on Eddystone beacons, see [Google's Eddystone repository](https://github.com/google/eddystone/tree/master/eddystone-url).  

Initializing Advertisement

```
gecko_cmd_le_gap_bt5_set_adv_data(0, 0, EDDYSTONE_DATA_LEN, eddystone_data)
```
The first two parameters are the advertising set handle and a value indicating that data is to be used in advertising packets.

```
gecko_cmd_le_gap_set_advertise_timing(0, 160, 160, 0, 0);

```
The following call sets the minimum and maximum advertising interval to 100 ms ( 625 us * 160). All three advertising channels are used by default (37, 38, 39).

```
gecko_cmd_le_gap_start_advertising(0, le_gap_user_data, le_gap_connectable_scannable);

```
The device is now ready to start advertising the Eddystone-URL beacon data.

#### Payload for Eddystone Advertisement

0x03,          // Length of service list  
0x03,          // Service list  
0xAA, 0xFE,    // Eddystone ID  
0x10,          // Length of service data  
0x16,          // Service data  
0xAA,  0xFE,   // Eddystone ID  
0x10,          // Frame type Eddystone-URL  
0x00,          // Tx power  
0x00,          // http://www., 0x01=https://www.
's','i','l','a','b','s','.','c','o','m'

## USART Implementation

GPIO and UART clock are enable for the communication of USART module and laptop. USART must be initialized with the user specific data such as Baudrate, data bits, parity bit and stop bit.
Here we have initialized as default.

### Transmit Data

```
USART_Enable(USART0, usartEnable)
```
The data is transferred by raising the flag of desired UART pot and Tx pin.
It is kept in a loop to transfer all data that has been assigned to a  variable. Instead of USART0 any USART port can be assigned.

```
USART_Tx(USART0, TXData[i]);
```
Here TxData is user defined character array with predefined value.

### Receive data

Receiving data packets from the generic computer system is done by enabling the Rx pin of UART.
GPIO external pin interrupt is set up to enable interrupt on the rising edge or the falling edge.

```
GPIO_ExtIntConfig(gpioPortA, 0, 0, true, false, true);
```
Note: A callback function is a function passed into another function as an argument, which is then invoked inside the outer function to complete some kind of routine or action.

Thus callback function is provoked to read all data that is being catched by usart module on USART_Rx pin.

### Connection to Slave

As we have developed UART application we will receive command from uart and connect to user given address which is being used as a slave device. We need to convert ascii to hexadecimal for identifying the bluetooth address.After we convert the bluetooth address in hexadecimal, it is connected with the master device.
````
BD Address of Remote Device :D0CF5EB2599C
````
Though this Connection will be established between both the devices and then it will discover services and characteristics of the slave device.
AS in this demo application we need to read temprerature from the Slave device we will need to find specifically that characteristic.

#### Custom GATT Characteristic

We need to add new service in the GATT configurator. We need Temperature characteristic so we can simply drag Temperature Characteristic Value from the inbuilt characteristic define all their parameters such as value, value type as user and value length.

##### Write Characteristic
Once we find the temperature characteristic of the slave device.
We will get the value of that temperature by initiating soft timer and read the temperature of slave device. Interval after getting each value is given by user through uart "Get Temp x;", where x represents the number of seconds.
````
UUID of Temperature Characteristics : c976c595-2f1a-46e7-bbf8-8976b3812832
````

###### LED Indication
LED is set on the Slave device for every reading of temperature that we get from the remote device. In that way we can verify if we are getting the readings or not.
