/***************************************************************************//**
 * @file app.c
 * @brief Silicon Labs Empty Example Project
 *
 * This example demonstrates the bare minimum needed for a Blue Gecko C application
 * that allows Over-the-Air Device Firmware Upgrading (OTA DFU). The application
 * starts advertising after boot and restarts advertising after a connection is closed.
 *******************************************************************************
 * # License
 * <b>Copyright 2018 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

/* Bluetooth stack headers */
#include "bg_types.h"
#include "native_gecko.h"
#include "gatt_db.h"
#include "stdio.h"
#include "stdlib.h"
#include "stdbool.h"
#include "app.h"
#include "tempdrv.h"

#include "em_device.h"
#include "em_chip.h"
#include "gpiointerrupt.h"
#include "infrastructure.h"

//Eddystone data
#define EDDYSTONE_DATA_LEN (21)

static const uint8_t eddystone_data[EDDYSTONE_DATA_LEN] = { 0x03, // Length of service list
		0x03,          // Service list
		0xAA, 0xFE,    // Eddystone ID
		0x10,          // Length of service data
		0x16,          // Service data
		0xAA, 0xFE,   // Eddystone ID
		0x10,          // Frame type Eddystone-URL
		0x00,          // Tx power
		0x01,          // 0x00=http://www., 0x01=https://www.
		's', 'i', 'l', 'a', 'b', 's', '.', 'c', 'o', 'm' };

//UART Dependencies
#include <em_usart.h>
#include <em_cmu.h>
#include <em_gpio.h>

#define UART_TX_PORT	gpioPortA
#define UART_TX_PIN		0
#define UART_TX_LOC		0
#define UART_RX_PORT	gpioPortA
#define UART_RX_PIN		1
#define UART_RX_LOC		0
#define PERR(x) printf("BG call error - <0x%04x>\n", (x))
#define BGC(x)  if ( (x) != bg_err_success ) { PERR((x)); }

char UART_RX_BUFFER[255];
char CONNECTION_OPENED[] = "Connection Opened\r\n";
char CONNECTED[] = "Connected to :";
char GIVEN_SEC[] = "\r\nSubscribed Temp every (sec):";
uint8_t TEMPERATURE_SERVICE_UUID[] = { 0x3a, 0x3d, 0xb0, 0x72, 0x89, 0x92, 0x63,
		0x98, 0xca, 0x44, 0xdb, 0x20, 0x9a, 0xd4, 0x5a, 0x49 };
bd_addr uart_input_macaddr;

bool master;

uint8_t conn_var = 0;
uint8_t temp_var = 0;
uint8_t var = 0;
uint8_t* charValue;
uint32_t temperature;
uint8_t user_char_buf[4] = { 0x02, 0x04, 0x08, 0x0A };
uint8_t uart_received_data_len = 0;
uint8_t temp_time = 0;
gatt_procedure current_state;
char tempbuffer[20];

/* Print boot message */
static void bootMessage(struct gecko_msg_system_boot_evt_t *bootevt);
void uart_initialize();
void uart_receive(uint8_t onPin);
void uart_send(char* p_data, uint16_t len);
int8_t read_internal_temperature();
struct gecko_msg_le_gap_connect_rsp_t ret_connect;
struct gecko_msg_le_gap_connect_rsp_t* p_ret_connect = &ret_connect;
struct gecko_msg_gatt_characteristic_evt_t remote_temp_char;

/* Flag for indicating DFU Reset must be performed */
static uint8_t boot_to_dfu = 0;

/* Main application */
void appMain(gecko_configuration_t *pconfig) {

	/* Initialize debug prints. Note: debug prints are off by default. See DEBUG_LEVEL in app.h */
#if !defined(__CROSSWORKS_ARM) && defined(__GNUC__)
	setvbuf(stdout, NULL, _IONBF, 0); /*Set unbuffered mode for stdout (newlib)*/
	RETARGET_SwoInit();
#endif
	/* Initialize stack */
	gecko_init(pconfig);
	uart_initialize();
	while (1) {
		/* Event pointer for handling events */
		struct gecko_cmd_packet* evt;

		/* if there are no events pending then the next call to gecko_wait_event() may cause
		 * device go to deep sleep. Make sure that debug prints are flushed before going to sleep */
		if (!gecko_event_pending()) {
		}

		/* Check for stack event. This is a blocking event listener. If you want non-blocking please see UG136. */
		evt = gecko_wait_event();

		/* Handle events */
		switch (BGLIB_MSG_ID(evt->header)) {
		/* This boot event is generated when the system boots up after reset.
		 * Do not call any stack commands before receiving the boot event.
		 * Here the system is set to start advertising immediately after boot procedure. */
		case gecko_evt_system_boot_id:

			bootMessage(&(evt->data.evt_system_boot));
			printf("boot event - starting advertising\r\n");
			gecko_cmd_system_set_tx_power(0);
			/* Set advertising parameters. 100ms advertisement interval.
			 * The first parameter is advertising set handle
			 * The next two parameters are minimum and maximum advertising interval, both in
			 * units of (milliseconds * 1.6).
			 * The last two parameters are duration and maxevents left as default. */
			gecko_cmd_le_gap_set_advertise_timing(0, 160, 160, 0, 0);
			gecko_cmd_le_gap_bt5_set_adv_data(0, 0, EDDYSTONE_DATA_LEN,
					eddystone_data);

			GPIO_PinModeSet(BSP_LED0_PORT, BSP_LED0_PIN, gpioModePushPull, 1);

			/* Start general advertising and enable connections. */
			gecko_cmd_le_gap_start_advertising(0, le_gap_user_data,
					le_gap_connectable_scannable);

			master = false;
			break;

		case gecko_evt_system_external_signal_id:
			if (evt->data.evt_system_external_signal.extsignals
					== UART_ONE_LINE_RECEIVED_EVENT) {
				//uart_send(&UART_RX_BUFFER[0], uart_received_data_len);
				conn_var = aide_findSubString(UART_RX_BUFFER, "Connect ");
				temp_var = aide_findSubString(UART_RX_BUFFER, "Get Temp ");
				if (conn_var != 0) {
					// Convert
					uint8_t ret_hex;
					ret_hex = ascii2hex(&UART_RX_BUFFER[conn_var],
							&(uart_input_macaddr.addr[0]));
					if (ret_hex == RESULT_FAILED_ODD_STR_CHAR) {
						printf("Failed due to wrong length for input data");
					} else if (ret_hex == RESULT_FAILED_OVER_LENGTH) {
						printf("Failed due to invalid character");
					} else if (ret_hex == RESULT_OK) {
						p_ret_connect = gecko_cmd_le_gap_connect(
								uart_input_macaddr, le_gap_address_type_public,
								le_gap_phy_1m);
						master = true;
						if (p_ret_connect->result == 0) {
							printf("Connection request successful\n");
						} else {
							printf(
									"Connection request failed & Error code is %d",
									p_ret_connect->result);
						}
					}

				}

				if (temp_var != 0) {
					char r_msg[] = "\nUpdating Temperature every (sec):";
					uart_send(&r_msg[0], sizeof(r_msg));
					uart_send(&UART_RX_BUFFER[temp_var],
							sizeof(UART_RX_BUFFER));
					temp_time = atoi(&UART_RX_BUFFER[temp_var]);
					// timer activation multishot on every second
					//gecko_cmd_hardware_set_soft_timer(32768 * temp_time, 0x01,0);
					printf("Temperature will be updated every %d secs.\n", temp_time);
					if (current_state == GATT_PROCEDURE_STATE_WRITE) {
						//printf("Writing & current state is  %d\n",current_state);
						printf("Result for Writing Command is %x\n",gecko_cmd_gatt_write_characteristic_value(
								remote_temp_char.connection,
								remote_temp_char.characteristic, 1, &temp_time)->result);
					}
				}
			}
			memset(UART_RX_BUFFER, 0x00, sizeof(UART_RX_BUFFER));
			uart_received_data_len = 0;

			break;

		case gecko_evt_le_connection_opened_id:

			printf("Connection Opened\r\n");
			if(master){
				uart_send(&CONNECTION_OPENED[0], sizeof(CONNECTION_OPENED));

			printf("Result of finding primary services: %i \n",
					gecko_cmd_gatt_discover_primary_services(
							evt->data.evt_le_connection_opened.connection)->result);
			bd_addr addr = evt->data.evt_le_connection_opened.address;
			uart_send(&CONNECTED[0], sizeof(CONNECTED));
			aide_hex_to_string(&(addr.addr[0]), sizeof(addr.addr),
					(uint8_t*) &UART_RX_BUFFER[0]);
			uart_send(&UART_RX_BUFFER[0], sizeof(UART_RX_BUFFER));
			}

			break;

		case gecko_evt_gatt_service_id:
			if (memcmp(&(evt->data.evt_gatt_service.uuid.data[0]),
					&(TEMPERATURE_SERVICE_UUID[0]),
					sizeof(TEMPERATURE_SERVICE_UUID)) == 0) {
				printf("\nService UUID : 0x");
				for (temp_var = evt->data.evt_gatt_service.uuid.len;
						temp_var != 0; temp_var--) {
					printf("%02x",
							evt->data.evt_gatt_service.uuid.data[temp_var - 1]);

				}
				printf("\n");
				gecko_cmd_gatt_discover_characteristics(
						evt->data.evt_gatt_service.connection,
						evt->data.evt_gatt_service.service);
			}

			break;

		case gecko_evt_gatt_characteristic_id:
			remote_temp_char.characteristic = evt->data.evt_gatt_characteristic.characteristic;
			remote_temp_char.connection = evt->data.evt_gatt_characteristic.connection;
			memcpy(&(remote_temp_char.uuid.data[0]),&(evt->data.evt_gatt_characteristic.uuid.data[0]),evt->data.evt_gatt_characteristic.uuid.len);
			printf("\nCharacteristics UUID : 0x");
			for (temp_var = evt->data.evt_gatt_characteristic.uuid.len;
					temp_var != 0; temp_var--) {
				printf("%02x",
						evt->data.evt_gatt_characteristic.uuid.data[temp_var - 1]);
			}
			printf("\n");
			current_state = GATT_PROCEDURE_STATE_READ;
			break;

			//---------------------------- At Slave ---------------------------//

		case gecko_evt_hardware_soft_timer_id:
			if (evt->data.evt_hardware_soft_timer.handle == 0x01) {
				printf("Current Temperature is %i\n",
						read_internal_temperature());
				GPIO_PinOutClear(BSP_LED0_PORT, BSP_LED0_PIN);
				gecko_cmd_hardware_set_soft_timer(32768*1,0x02,1);

			}
				else if(evt->data.evt_hardware_soft_timer.handle == 0x02){
				GPIO_PinOutSet(BSP_LED0_PORT, BSP_LED0_PIN);
			}

			break;
			//---------------------------- At Master ---------------------------//
		case gecko_evt_gatt_characteristic_value_id:
			// Send confirmation for the indication
			if(current_state == GATT_PROCEDURE_STATE_READ){
				//printf("I read value for temp char: %d\n",evt->data.evt_gatt_characteristic_value.value.data[0]);
				current_state = GATT_PROCEDURE_STATE_NOTIFY;
			}else if(current_state == GATT_PROCEDURE_STATE_NOTIFY){
				//printf("I notified value for temp char: %d\n",evt->data.evt_gatt_characteristic_value.att_opcode);
				current_state = GATT_PROCEDURE_STATE_WRITE;
			}else{
				uart_send("Temperature on Remote is: ",26);
				itoa((evt->data.evt_gatt_characteristic_value.value.data[0]),&tempbuffer[0],10);
				uart_send(&tempbuffer[0],2);
				uart_send("\r\n",2);
				printf("I got notified temp from remote : %d\n",evt->data.evt_gatt_characteristic_value.value.data[0]);
			}
			break;

		case gecko_evt_gatt_server_user_read_request_id:
			printf("gecko_evt_gatt_server_user_read_request_id\n");
			uint16_t ret;
			if (evt->data.evt_gatt_server_user_read_request.characteristic
					== gattdb_TEMPERATURE_VALUE) {
				/* For simplicity, not consider the offset > length of the buffer situation */
				ret =
						gecko_cmd_gatt_server_send_user_read_response(
								evt->data.evt_gatt_server_user_read_request.connection,
								gattdb_TEMPERATURE_VALUE, bg_err_success,
								sizeof(user_char_buf)
										- evt->data.evt_gatt_server_user_read_request.offset,
								user_char_buf
										+ evt->data.evt_gatt_server_user_read_request.offset)->result;
				BGC(ret);
			}
			break;

		case gecko_evt_le_connection_closed_id:

			printf("Connection Closed, reason: 0x%2.2x\r\n",
					evt->data.evt_le_connection_closed.reason);

			/* Check if need to boot to OTA DFU mode */
			if (boot_to_dfu) {
				/* Enter to OTA DFU mode */
				gecko_cmd_system_reset(2);
			} else {
				/* Restart advertising after client has disconnected */
				gecko_cmd_le_gap_start_advertising(0,
						le_gap_general_discoverable,
						le_gap_connectable_scannable);

			}
			gecko_cmd_hardware_set_soft_timer(0, 0x01, 0);
			break;

		case gecko_evt_gatt_procedure_completed_id:
			if (current_state == GATT_PROCEDURE_STATE_NOTIFY) {
				printf("Result for Set Characteristic Notification is %x\n",
						gecko_cmd_gatt_set_characteristic_notification(
								remote_temp_char.connection,
								remote_temp_char.characteristic,
								gatt_notification)->result);
				uart_send("\r\nNotification Subscribed\n", 25);
				current_state = GATT_PROCEDURE_STATE_WRITE;

			} else if (current_state == GATT_PROCEDURE_STATE_WRITE) {
				//uart_send(&GIVEN_SEC[0],sizeof(GIVEN_SEC));
				//current_state = GATT_PROCEDURE_STATE_IDLE;

			} else if(current_state == GATT_PROCEDURE_STATE_READ){
				printf("Result for Read Command %x\n",gecko_cmd_gatt_read_characteristic_value(remote_temp_char.connection,remote_temp_char.characteristic)->result);
			}


			break;
			/* Events related to OTA upgrading
			 ----------------------------------------------------------------------------- */

			/* Check if the user-type OTA Control Characteristic was written.
			 * If ota_control was written, boot the device into Device Firmware Upgrade (DFU) mode. */
		case gecko_evt_gatt_server_user_write_request_id:

			if (evt->data.evt_gatt_server_user_write_request.characteristic
					== gattdb_ota_control) {
				/* Set flag to enter to OTA mode */
				boot_to_dfu = 1;
				/* Send response to Write Request */
				gecko_cmd_gatt_server_send_user_write_response(
						evt->data.evt_gatt_server_user_write_request.connection,
						gattdb_ota_control, bg_err_success);

				/* Close connection to enter to DFU OTA mode */
				gecko_cmd_le_connection_close(
						evt->data.evt_gatt_server_user_write_request.connection);
			}
			/* TAG: Example of handling write request for writing a characteristic with "user" type */
			else if (evt->data.evt_gatt_server_user_write_request.characteristic
					== gattdb_TEMPERATURE_VALUE) {
				printf("Slave will notify temperature every %d sec\n",evt->data.evt_gatt_server_user_write_request.value.data[0]);
				gecko_cmd_hardware_set_soft_timer(
						32768*(evt->data.evt_gatt_server_user_write_request.value.data[0]),
						0x01, 0);

			} else {
				ret =
						gecko_cmd_gatt_server_send_user_write_response(
								evt->data.evt_gatt_server_user_write_request.connection,
								evt->data.evt_gatt_server_user_write_request.characteristic,
								(uint8) bg_err_att_invalid_att_length)->result;
				BGC(ret);
			}

			break;

			/* Add additional event handlers as your application requires */

		default:
			break;
		}
	}
}

/* Print stack version and local Bluetooth address as boot message */
static void bootMessage(struct gecko_msg_system_boot_evt_t *bootevt) {
#if DEBUG_LEVEL
	bd_addr local_addr;
	int i;

	printf("stack version: %u.%u.%u\r\n", bootevt->major, bootevt->minor,
			bootevt->patch);
	local_addr = gecko_cmd_system_get_bt_address()->address;

	printf("local BT device address: ");
	for (i = 0; i < 5; i++) {
		printf("%2.2x:", local_addr.addr[5 - i]);
	}
	printf("%2.2x\r\n", local_addr.addr[0]);
#endif
}
//---------------------------------  UART Initialization --------------------------------------//

void uart_initialize() {
	CMU_ClockEnable(cmuClock_USART0, true);
	CMU_ClockEnable(cmuClock_GPIO, true);

	USART_InitAsync_TypeDef initAsync = USART_INITASYNC_DEFAULT;
	initAsync.enable = false;
	USART_InitAsync(USART0, &initAsync);

	/* Set up RX pin */
	USART0->ROUTELOC0 = (USART0->ROUTELOC0 & (~_USART_ROUTELOC0_RXLOC_MASK))
			| UART_RX_LOC; //USART0_RX
	USART0->ROUTEPEN = USART0->ROUTEPEN | USART_ROUTEPEN_RXPEN;
	/* Set up TX pin */
	USART0->ROUTELOC0 = (USART0->ROUTELOC0 & (~_USART_ROUTELOC0_TXLOC_MASK))
			| UART_TX_LOC; //USART0_TX
	USART0->ROUTEPEN = USART0->ROUTEPEN | USART_ROUTEPEN_TXPEN;

	GPIO_PinModeSet(UART_TX_PORT, UART_TX_PIN, gpioModePushPull, 1);
	GPIO_PinModeSet(UART_RX_PORT, UART_RX_PIN, gpioModeInput, 1);
	GPIOINT_Init();

	GPIOINT_CallbackRegister(UART_RX_PIN, uart_receive);
	GPIO_ExtIntConfig(UART_RX_PORT, UART_RX_PIN, UART_RX_PIN, false, true,
	true);
	GPIOINT_Init();
	USART_Enable(USART0, usartEnable);
	printf("uart_initialize()\n");
}
//---------------------------- UART Receiving ---------------------------//

void uart_receive(uint8_t onPin) {
	BUS_RegBitWrite(&(GPIO->IEN), onPin, false); //to perform a single-bit write operation on a peripheral register.
	if (onPin == UART_RX_PIN) {
		while (USART0->STATUS & USART_STATUS_RXDATAV) {
			UART_RX_BUFFER[uart_received_data_len] = (char) USART0->RXDATA;
			if (UART_RX_BUFFER[uart_received_data_len] == ';') {
				printf("Valid command.");
				gecko_external_signal(UART_ONE_LINE_RECEIVED_EVENT);
				break;
			} else {
				if (uart_received_data_len
						> (sizeof(UART_RX_BUFFER) / sizeof(char))) {
					printf("Buffer Overflow: %s\n", UART_RX_BUFFER);
					memset(UART_RX_BUFFER, 0x00, sizeof(UART_RX_BUFFER));
					uart_received_data_len = 0;
				} else {
					uart_received_data_len++;
				}
			}
		}
		// Clear any pending interrupt
		GPIO->IFC = 1 << 0;
		BUS_RegBitWrite(&(GPIO->IEN), onPin, true);
	}
}
//---------------------------- UART Sending ---------------------------//

void uart_send(char* p_data, uint16_t len) {
	while (len != 0) {
		USART_Tx(USART0, *(p_data));
		len--;
		p_data++;
	}
}
//---------------------------- Finding End of Sub String Position ---------------------------//

uint8_t aide_findSubString(char main_string[], const char sub_string[]) {
	uint16_t l, i, j;
//finding length of second string
//printf("\n%s\n%s\n",main_string,sub_string);

	for (l = 0; sub_string[l] != '\0'; l++) {
	}
// printf("sub string length = %i\n",l);
// printf("found : ");
	for (i = 0, j = 0; main_string[i] != '\0' && sub_string[j] != '\0'; i++) {
		if (main_string[i] == sub_string[j]) {
			j++;
		} else {
			j = 0;
		}
// printf("%i",j);
	}
	printf("\n");
	if (j == l) {
		return i; // return position of end of substring
	} else {
		return 0; // no sub string found
	}
}

//---------------------------- ASCII to Hex ---------------------------//

uint8_t ascii2hex(char* string, uint8_t* data) {

	if (string == NULL) {
		return 0;
	}
	uint16_t slength = strlen(string) - 1;

	if ((slength % 2) != 0) { // slength must be even.
		printf("slength is %d\n", slength);
		return RESULT_FAILED_ODD_STR_CHAR;
	}
	size_t index = 0;
	while (index < slength) { // executed until the index is greater than string length
		char c = string[index];
		int value = 0;
		if (c >= '0' && c <= '9') {
			value = (c - '0');
		} else if (c >= 'A' && c <= 'F') {
			value = (10 + (c - 'A'));
		} else if (c >= 'a' && c <= 'f') {
			value = (10 + (c - 'a'));
		} else {
			printf("RESULT_FAILED_OVER_LENGTH : %s\n", string);
			return RESULT_FAILED_OVER_LENGTH;
		}

		data[(index / 2)] = data[(index / 2)]
				+ (value << (((index + 1) % 2) * 4)); // adding the value to the same index value and shifted by 4 (equivalent to multiplying by 16).
		index++;
	}

	return RESULT_OK;
}
//---------------------------- Find New Line ---------------------------//

/*uint8_t aide_findNewLine(char main_string[]) {
 uint16_t l;
 //finding length of string
 //printf("%s\n",main_string);

 for (l = 0; l < sizeof(main_string)/sizeof(char); l++) {
 if (main_string[l] == '\n') {
 break;
 }
 }
 if (main_string[l] == '\n') {
 return l; // return position of end of string
 } else {
 return 0; // no sub string found
 }
 }
 */

//---------------------------- ASCII to Integer ---------------------------//

uint8_t ascii2int(char* data) {
	int sum, digit, i;
	sum = 0;
	for (i = 0; i < strlen(data) - 1; i++) {
		digit = data[i] - 0x30;
		sum = (sum * 10) + digit;
	}
	return sum;
}
//---------------------------- Hex to ASCII ---------------------------//

void aide_hex_to_string(uint8_t* in, size_t in_size, uint8_t* out) {
	uint8_t i;
	uint8_t hex_string[] = "0123456789ABCDEF";
	for (i = 0; i < in_size; i++) {
		out[i * 2] = hex_string[in[i] >> 4];
		out[i * 2 + 1] = hex_string[in[i] & 0x0F];
	}
}
//---------------------------- Reading Internal Temperature -----------------------------//

int8_t read_internal_temperature() {
	TEMPDRV_Init();
	int8_t temp = TEMPDRV_GetTemp();
	TEMPDRV_DeInit();
	gecko_cmd_gatt_server_send_characteristic_notification(
			0xFF, gattdb_TEMPERATURE_VALUE, 1,
			(const uint8*) &temp);
	return temp;
}

